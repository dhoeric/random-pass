# random-pass
Simple command to generate password

## Usage
```
$ random-pass -l 32
VJ3YI43JahEY6VtmrUJJziZT8HFYi1Ep
```

## Install
```
go get gitlab.com/dhoeric/random-pass
```
