package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"

	"gopkg.in/urfave/cli.v1"
)

func main() {
	var passwordLen int

	app := cli.NewApp()
	app.Name = "random-pass"
	app.Usage = "generate a random password in given length"
	app.Version = "0.0.2"

	app.Flags = []cli.Flag{
		cli.IntFlag{
			Name:        "length, l",
			Value:       32,
			Usage:       "lenght of the random password",
			Destination: &passwordLen,
		},
	}

	app.Action = func(c *cli.Context) error {
		rand.Seed(time.Now().UTC().UnixNano())

		dict := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"

		for i := 0; i < passwordLen; i++ {
			idx := rand.Intn(len(dict))
			fmt.Print(string(dict[idx]))
		}

		return nil
	}

	app.Run(os.Args)
}
